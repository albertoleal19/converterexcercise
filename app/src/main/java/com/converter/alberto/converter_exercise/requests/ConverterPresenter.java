package com.converter.alberto.converter_exercise.requests;

import com.converter.alberto.converter_exercise.models.RatesResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by Alberto on 11/6/2017.
 */

public interface ConverterPresenter {

    @GET("latest?base=USD")
    Observable<RatesResponse> getRates();

}
