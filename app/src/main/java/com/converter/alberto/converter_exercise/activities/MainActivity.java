package com.converter.alberto.converter_exercise.activities;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.converter.alberto.converter_exercise.R;
import com.converter.alberto.converter_exercise.adapters.BaseGenericAdapter;
import com.converter.alberto.converter_exercise.adapters.GenericAdapterFactory;
import com.converter.alberto.converter_exercise.adapters.GenericItemView;
import com.converter.alberto.converter_exercise.adapters.RateItemView;
import com.converter.alberto.converter_exercise.models.RateItem;
import com.converter.alberto.converter_exercise.models.Rates;
import com.converter.alberto.converter_exercise.requests.ConverterPresenterImpl;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity implements ConverterPresenterImpl.ConverterViewInterface{

    private ConverterPresenterImpl presenter;

    private BaseGenericAdapter adapter;
    List<RateItem> rateItems = new ArrayList<>();

    @BindView(R.id.input_layout_amount)
    TextInputLayout layoutInput;

    @BindView(R.id.edit_text_amount)
    EditText etAmountToConvert;

    @BindView(R.id.rv_rates)
    RecyclerView rvRates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new ConverterPresenterImpl(this);

        initView();

    }

    private void initView() {

        rateItems.add(new RateItem(0d, Rates.Currency.BRL));
        rateItems.add(new RateItem(0d, Rates.Currency.GBP));
        rateItems.add(new RateItem(0d, Rates.Currency.EUR));
        rateItems.add(new RateItem(0d, Rates.Currency.JPY));
        adapter = new BaseGenericAdapter(new GenericAdapterFactory() {
            @Override
            public GenericItemView onCreateViewHolder(ViewGroup parent, int viewType) {
                return new RateItemView(MainActivity.this);
            }
        });

        rvRates.setLayoutManager(new LinearLayoutManager(this));
        rvRates.setItemAnimator(new DefaultItemAnimator());
        adapter.setItems(rateItems);
        rvRates.setAdapter(adapter);

    }

    private void onConvert(){
        try {
            int amount = Integer.parseInt(etAmountToConvert.getText().toString());
            presenter.getRates(amount);
        } catch (NumberFormatException ex) {
            layoutInput.setError(getResources().getString(R.string.invalid_format));
            Log.e("INPUT_CAST", "ERROR");
        }
    }

    @OnClick(R.id.button_convert)
    void onConvertClick() {
        if(isInputValid())
            onConvert();
    }

    @OnTextChanged(R.id.edit_text_amount)
    void onEditTextValueChanged() {
        if(!TextUtils.isEmpty(layoutInput.getError()))
            layoutInput.setError("");
    }

    private boolean isInputValid() {
        if(TextUtils.isEmpty(etAmountToConvert.getText().toString()) || !TextUtils.isDigitsOnly(etAmountToConvert.getText().toString())) {
            layoutInput.setError(getResources().getString(R.string.invalid_format));
            return false;
        }
        layoutInput.setError("");
        return true;
    }

    @Override
    public void onSuccess(List<RateItem> rates) {
        adapter.setItems(rates);
    }

    @Override
    public void onError() {
        Toast.makeText(this, "Error getting the info", Toast.LENGTH_LONG).show();
    }
}
