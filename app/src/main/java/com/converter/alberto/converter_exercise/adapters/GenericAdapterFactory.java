package com.converter.alberto.converter_exercise.adapters;

import android.view.ViewGroup;

/**
 * Created by Alberto on 11/6/2017.
 */
@SuppressWarnings("ALL")
public abstract class GenericAdapterFactory {
    public abstract GenericItemView onCreateViewHolder(ViewGroup parent, int viewType);
}
