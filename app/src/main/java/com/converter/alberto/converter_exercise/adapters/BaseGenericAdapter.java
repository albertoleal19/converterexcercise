package com.converter.alberto.converter_exercise.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alberto on 11/6/2017.
 */
@SuppressWarnings("ALL")
public class BaseGenericAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected List<GenericItem> items;
    protected GenericAdapterFactory factory;

    public BaseGenericAdapter(GenericAdapterFactory factory) {
        this.factory = factory;
        this.items = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = (View) this.factory.onCreateViewHolder(parent, viewType);
        return new RecyclerView.ViewHolder(view) {
            @Override
            public String toString() {
                return super.toString();
            }
        };
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder.itemView instanceof GenericItemView) {
            GenericItemView genericItemView = (GenericItemView) holder.itemView;
            genericItemView.bind(items.get(position));
        }
    }

    @Override
    public int getItemCount() {

        return items != null ? items.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position).getType();
    }

    public <T extends GenericItem> T getItem(int index) {
        try {
            return  (T) this.items.get(index);
        }catch (IndexOutOfBoundsException ex){
            Log.i("ADAPTER_GETITEM_ERROR",ex.getMessage());
        }
        return null;
    }

    public void setItems(List<? extends GenericItem> items) {
        this.items = new ArrayList<>();
        if (items != null) {
            for (GenericItem genericItem : items) {
                this.items.add(genericItem);
            }
        } else {
            this.items = new ArrayList<>();
        }
        notifyDataSetChanged();
    }

    public void update(GenericItem item) {
        int index = items.indexOf(item);
        if (index != -1) {
            items.set(index, item);
            notifyItemChanged(index);
        }
    }

    public void remove(GenericItem item) {
        int index = items.indexOf(item);
        if (index != -1) {
            items.remove(index);
            notifyItemRemoved(index);
        }
    }

    public void remove(int index) {
        if (index != -1 && index < items.size()) {
            items.remove(index);
            notifyItemRemoved(index);
        }
    }

    public void addItem(GenericItem item) {
        items.add(item);
        notifyDataSetChanged();
    }

    public void addItem(int index, GenericItem item) {
        if (items == null) {
            items = new ArrayList<>();
        }
        items.add(index, item);
        notifyItemInserted(index);
    }

    public void addItems(List<? extends GenericItem> itemList) {
        items.addAll(itemList);
        notifyDataSetChanged();
    }

    public void clear() {
        if (items != null) {
            items.clear();
            notifyDataSetChanged();
        }
    }
}
