package com.converter.alberto.converter_exercise.requests;

import android.content.Context;
import android.util.Log;

import com.converter.alberto.converter_exercise.R;
import com.converter.alberto.converter_exercise.models.RateItem;
import com.converter.alberto.converter_exercise.models.Rates;
import com.converter.alberto.converter_exercise.models.RatesResponse;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Alberto on 11/6/2017.
 */

public class ConverterPresenterImpl {

    private ConverterPresenter converterInterface;
    private ConverterViewInterface view;

    private int amountToConvert = 0;

    public ConverterPresenterImpl(Context context) {
        converterInterface = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(context.getString(R.string.base_url))
                .build()
                .create(ConverterPresenter.class);

        if(context instanceof ConverterViewInterface)
            view = (ConverterViewInterface)context;
    }

    public void getRates(int amount) {
        amountToConvert = amount;
        converterInterface.getRates()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RatesResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(RatesResponse response) {
                        onRatesResponseSuccess(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        onRatesResponseError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void onRatesResponseSuccess(RatesResponse rates) {
        List<RateItem> rateItems = new ArrayList<>();
        rateItems.add(new RateItem(rates.getRates().getBRL() * amountToConvert, Rates.Currency.BRL));
        rateItems.add(new RateItem(rates.getRates().getGBP() * amountToConvert, Rates.Currency.GBP));
        rateItems.add(new RateItem(rates.getRates().getEUR() * amountToConvert, Rates.Currency.EUR));
        rateItems.add(new RateItem(rates.getRates().getJPY() * amountToConvert, Rates.Currency.JPY));

        if(view != null)
            view.onSuccess(rateItems);

        Log.i("RATES_RESPONSE", new Gson().toJson(rates));
    }

    private void onRatesResponseError(Throwable e) {
        if(view != null)
            view.onError();
        Log.e("RATES_RESPONSE_ERROR", e.getMessage());
    }

    public interface ConverterViewInterface {

        void onSuccess(List<RateItem> rates);

        void onError();
    }
}
