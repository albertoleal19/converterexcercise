package com.converter.alberto.converter_exercise.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Alberto on 11/6/2017.
 */

public class Rates {

    @SerializedName("BRL")
    @Expose
    private Double brl;
    @SerializedName("GBP")
    @Expose
    private Double gbp;
    @SerializedName("JPY")
    @Expose
    private Double jpy;
    @SerializedName("EUR")
    @Expose
    private Double eur;

    public Double getBRL() {
        return brl;
    }

    public void setBRL(Double bRL) {
        this.brl = bRL;
    }

    public Double getGBP() {
        return gbp;
    }

    public void setGBP(Double gBP) {
        this.gbp = gBP;
    }

    public Double getJPY() {
        return jpy;
    }

    public void setJPY(Double jPY) {
        this.jpy = jPY;
    }

    public Double getEUR() {
        return eur;
    }

    public void setEUR(Double eUR) {
        this.eur = eUR;
    }

    public enum Currency {

        GBP("GBP"), BRL("BRL"), JPY("JPY"), EUR("EUR");

        private String currency;

        private Currency(String currency){
            this.currency = currency;
        }

        public String getCurrency(){
            return this.currency;
        }

        public void setStatusApply(String currency){
            this.currency = currency;
        }

        public String toString(){
            return this.currency;
        }
    }
}