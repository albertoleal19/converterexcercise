package com.converter.alberto.converter_exercise.adapters;

/**
 * Created by Alberto on 11/6/2017.
 */
public interface GenericItemView {
    void bind(GenericItem item);
}
