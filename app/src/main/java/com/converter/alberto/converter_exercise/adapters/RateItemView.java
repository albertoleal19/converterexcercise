package com.converter.alberto.converter_exercise.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.converter.alberto.converter_exercise.R;
import com.converter.alberto.converter_exercise.models.RateItem;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alberto on 11/6/2017.
 */

public class RateItemView extends RelativeLayout implements GenericItemView {
    @BindView(R.id.tv_rate)
    TextView tvRate;

    Context context;

    public RateItemView(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.row_rate, this);
        ButterKnife.bind(this);
    }

    @Override
    public void bind(GenericItem item) {

        RateItem rate = (RateItem) item;
        long roundedValue = Math.round(rate.getValue());
        tvRate.setText(String.format("%s %s", rate.getCurrency().toString(), String.valueOf(roundedValue)));
        switch (rate.getCurrency()) {
            case BRL:
                tvRate.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context,R.drawable.ic_brl), null,null, null);
                break;

            case GBP:
                tvRate.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context,R.drawable.ic_gbp), null,null, null);
                break;

            case JPY:
                tvRate.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context,R.drawable.ic_jpy), null,null, null);
                break;

            case EUR:
                tvRate.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context,R.drawable.ic_eur), null,null, null);
                break;

        }
    }
}
