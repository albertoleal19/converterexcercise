package com.converter.alberto.converter_exercise.models;

import com.converter.alberto.converter_exercise.adapters.GenericItem;

/**
 * Created by Alberto on 11/6/2017.
 */

public class RateItem  implements GenericItem{

    private Double value;

    private Rates.Currency currency;

    public RateItem() {
    }

    public RateItem(Double value, Rates.Currency type) {
        this.value = value;
        this.currency = type;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Rates.Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Rates.Currency type) {
        this.currency = type;
    }

    @Override
    public int getType() {
        return 0;
    }
}
