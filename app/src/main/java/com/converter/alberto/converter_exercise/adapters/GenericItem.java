package com.converter.alberto.converter_exercise.adapters;

/**
 * Created by Alberto on 11/6/2017.
 */
@SuppressWarnings("ALL")
public interface GenericItem {
    int getType();
}
